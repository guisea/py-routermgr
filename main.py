#!/usr/bin/env python

# Standard Libraries
import datetime
import telnetlib
import time

# Pypi Libraries
from ping3 import ping
import yaml
from tendo import singleton
import slack


def main():
    # Ensure we are only running once
    me = singleton.SingleInstance()
    # Get Config file
    config = []
    with open("config/app.yml", 'r') as stream:
        try:
            config = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
    r = ping('8.8.8.8')
    detected = ''
    restored = ''

    if r is not None and r is not False:
        print("Able to ping Google. Yay!!")
        pass
    else:
        # Maybe something funny is happening here
        detected = datetime.datetime.now()
        time.sleep(10)
        r = ping('8.8.4.4')
        if r is not None and r is not False:
            print("Able to ping Google. Yay!!")
            pass
        else:
            # We are definitely not good here
            resetWAN(config)
            time.sleep(20)
            response = False
            # Entering loop to restart WAN every 60 seconds
            while not response:
                r = ping('1.1.1.1')
                if r is not None and r is not False:
                    # Internet is restored
                    response = True
                    restored = datetime.datetime.now()
                    message = ''.join(["WAN Down: ",
                                       detected.strftime("%d/%m/%Y, %H:%M:%S"),
                                       "\nWAN Restored: ",
                                       restored.strftime("%d/%m/%Y, %H:%M:%S"),
                                       "\n"])
                    sendNotification(config, message)
                else:
                    resetWAN(config)  # Try and reset the WAN link again
                    time.sleep(60)  # Sleep a minute


def resetWAN(config):
    user = config['router_username']
    password = config['router_password']

    tn = telnetlib.Telnet(config['router_hostname'])

    tn.read_until(b"login: ")
    tn.write(bytes(user + "\n", encoding='utf8'))
    if password:
        tn.read_until(bytes("Password: ", encoding='utf8'))
        tn.write(bytes(password + "\n", encoding='utf8'))
    tn.write(b"service restart_wan\n")
    tn.read_until(b'Done.')
    tn.write(b"exit\n")


def sendNotification(config, message):

    client = slack.WebClient(token=config['slack_token'])

    response = client.chat_postMessage(
        channel=config['slack_channel'],
        text=message)


if __name__ == "__main__":
    main()
