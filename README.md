# README #

### What is this repository for? ###

* This repository contains a Python application which will attempt to Ping via the WAN.  Upon failure it will logon to my Router Asus RT-N56U and restart the WAN link. 
  Gets around annoying connection resets by ISP in the wee hours :)

* Version 0.1

### How do I get set up? ###

* Checkout/Clone or Download this application
* Install dependencies `pip install -r requirements.txt`
* Copy config/app.yml.EXAMPLE to config/app.yml and configure with your relevant details.
  p.s Only Twilio notification on WAN link recovery is supported at this time.